<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Portal_Handgran
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<?php wp_head(); ?>
<head>

	<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
</head>


<header class="cabecalho">
	<div class="container">
		<div class="row">
			
			<div class="col-sm-3">
				<figure class="logo">
					<a href="<?php echo home_url('/'); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo copy.jpg">
					</a>
				</figure>
			</div>

			<div class="col-sm-6">
				<div class="areaPesquisa">
					<form action="#" method="get" accept-charset="utf-8" id="searchform" role="search">
						<input type="text" name="s" id="s" placeholder="O que você está procurando?">
						<input type="submit"  id="searchsubmit" name="">
					</form>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="menuUser">
					<div class="row">
						<?php $user = wp_get_current_user();  if($user):  ?>
						<div class="col-xs-8">

							<div class="userOption">
								<span style="background: <?php echo get_the_author_meta('admin_color'); ?>">Olá <strong><?php  if ($user->display_name) {
									echo $user->display_name;
								}else {echo "Visitante";}; ?></strong></span>
								
							</div>
						</div>
						<div class="col-xs-4">
							<figure>

								<?php if (esc_url( get_avatar_url( $user->ID ))) {
									$urlFoto = esc_url( get_avatar_url( $user->ID ));
								}else{
									$urlFoto = get_template_directory_uri()."/img/user.svg";
								} ?>
								<img src="<?php echo $urlFoto ?>">
							</figure>
							<nav class="menuOpition">
								<ul>
									<li>
										<a href="<?php echo  get_edit_user_link( $user_id ) ?> ">Minha Conta</a>
									</li>
									<li>
										<a href="<?php echo wp_logout_url(); ?>">Sair</a>
									</li>
								</ul>
							</nav>
						</div>
					<?php endif; ?>
					</div>
				</div>
			</div>

		</div>
	</div>
</header>

<body <?php body_class(); ?>>

