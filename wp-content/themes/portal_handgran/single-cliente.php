<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Portal_Handgran
 */

get_header();

?>

<div class="pg-cliente pg">
	<div class="containerFull">
	
		<div class="row">
			<div class="col-sm-6 paddingCorrecao">
				<div class="areaTexto">
					<figure>
						<?php 
							$urlLogo = rwmb_meta('Handgran_cliente_logo_empresa');
							foreach ($urlLogo as $urlLogo):
								$urlLogo = $urlLogo['full_url'];
						?>
						<img src="<?php echo $urlLogo  ?>" alt="<?php echo get_the_title(); ?>">
						<?php endforeach; ?>
					</figure>
					<h2><?php echo get_the_title() ?></h2>
					<?php while ( have_posts() ) : the_post(); echo the_content(); endwhile; ?>
				</div>
			</div>
			<div class="col-sm-6 paddingCorrecao">
				<div class="areaInfos areaTexto">
					<ul>
						<li>
							<strong>Cadastro de Cliente</strong>
							<div class="row">
								<div class="col-sm-4"><span>Fantasia: </span></div>
								<div class="col-sm-8"><?php echo $cliente_Fantasia_empresa = rwmb_meta('Handgran_cliente_Fantasia_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Razão social: </span></div>
								<div class="col-sm-8"><?php echo $cliente_RazaoSocial_empresa = rwmb_meta('Handgran_cliente_RazaoSocial_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>CNPJ: </span></div>
								<div class="col-sm-8"><?php echo $cliente_cnpj_empresa = rwmb_meta('Handgran_cliente_cnpj_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Inscrição estadual: </span></div>
								<div class="col-sm-8"><?php echo $cliente_inscricaoEstadual_empresa = rwmb_meta('Handgran_cliente_inscricaoEstadual_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Inscrição municipal: </span></div>
								<div class="col-sm-8"><?php echo $cliente_inscricaoMunicipal_empresa = rwmb_meta('Handgran_cliente_inscricaoMunicipal_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>E-mail para contato: </span></div>
								<div class="col-sm-8"><?php echo $cliente_email_contato_empresa = rwmb_meta('Handgran_cliente_email_contato_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Telefone para Contato: </span></div>
								<div class="col-sm-8"><?php echo $cliente_numero_contato_empresa = rwmb_meta('Handgran_cliente_numero_contato_empresa'); ?></div>
							</div>

						</li>
						<li>
							<strong>Cadastro de Cliente - Endereço</strong>

							<div class="row">
								<div class="col-sm-4"><span>Rua: </span></div>
								<div class="col-sm-8"><?php echo $cliente_rua_empresa = rwmb_meta('Handgran_cliente_rua_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Numero: </span></div>
								<div class="col-sm-8"><?php echo $cliente_numero_empresa = rwmb_meta('Handgran_cliente_numero_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Bairro: </span></div>
								<div class="col-sm-8"><?php echo $cliente_bairro_empresa = rwmb_meta('Handgran_cliente_bairro_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Cidade: </span></div>
								<div class="col-sm-8"><?php echo $cliente_cidade_empresa = rwmb_meta('Handgran_cliente_cidade_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Complemento: </span></div>
								<div class="col-sm-8"><?php echo $cliente_complemento_empresa = rwmb_meta('Handgran_cliente_complemento_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>CEP: </span></div>
								<div class="col-sm-8"><?php echo $cliente_CEP_empresa = rwmb_meta('Handgran_cliente_CEP_empresa'); ?></div>
							</div>

						</li>	
						<li>
							<strong>Cadastro de Cliente - Responsável</strong>

							<div class="row">
								<div class="col-sm-4"><span>Nome Completo - Responsável: </span></div>
								<div class="col-sm-8"><?php echo $cliente_responsavel_empresa = rwmb_meta('Handgran_cliente_responsavel_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Email - Responsável: </span></div>
								<div class="col-sm-8"><?php echo $cliente_Email_empresa = rwmb_meta('Handgran_cliente_Email_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Celular - Responsável: </span></div>
								<div class="col-sm-8"><?php echo $cliente_Celular_empresa = rwmb_meta('Handgran_cliente_Celular_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Contatos - Responsáveis: </span></div>
								<div class="col-sm-8">
									<?php 
									
										$cliente_contatos_empresa = rwmb_meta('Handgran_cliente_contatos_empresa'); 
										foreach ($cliente_contatos_empresa as $cliente_contatos_empresa) :
											$cliente_contatos_empresa = explode("|", $cliente_contatos_empresa);
										
									?>
									
										<div class="row">
											<div class="col-xs-6"><?php echo $cliente_contatos_empresa [0] ?></div>
											<div class="col-xs-6"><?php echo $cliente_contatos_empresa [1] ?><br><?php echo $cliente_contatos_empresa [2] ?></div>
											
										</div>

									<?php endforeach; ?>
										
								</div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>CPF - Responsável: </span></div>
								<div class="col-sm-8"><?php echo $cliente_CPF_empresa = rwmb_meta('Handgran_cliente_CPF_empresa'); ?></div>
							</div>

							<div class="row">
								<div class="col-sm-4"><span>Observação - Responsável: </span></div>
								<div class="col-sm-8"><?php echo $cliente_observacao_empresa = rwmb_meta('Handgran_cliente_observacao_empresa'); ?></div>
							</div>

						</li>
						<li>
							<strong>Cadastro de Cliente - Canais de acesso</strong>

							<div class="row">
								
								<div class="col-sm-12">
								<?php 
									$cliente_canal_empresa = rwmb_meta('cliente_canal_empresa'); 
									
									foreach ($cliente_canal_empresa as $cliente_canal_empresa):
								?>
									<div class="areaCanais">
										<div class="row">
											<div class="col-sm-12"><p class="tituloCanais"><?php echo $cliente_canal_empresa['canal'] ?></p></div>
										</div>
										<div class="row">
											<div class="col-sm-6"><strong>Url</strong></div>
											<div class="col-sm-6"><p><button  valorCampo="Clique para copiar!"class="btnCopiar"><?php echo $cliente_canal_empresa['url'] ?></button></p></div>
										</div>
										<div class="row">
											<div class="col-sm-6"><strong>Login</strong></div>
											<div class="col-sm-6"><p><button  valorCampo="Clique para copiar!"class="btnCopiar"><?php echo $cliente_canal_empresa['login'] ?></button></p></div>
										</div>
										<div class="row">
											<div class="col-sm-6"><strong>Senha</strong></div>
											<div class="col-sm-6"><p><button  valorCampo="Clique para copiar!"class="btnCopiar"><?php echo $cliente_canal_empresa['senha'] ?></button></p></div>
										</div>

										<div class="row">
											<div class="col-sm-6"><strong>Infos</strong></div>
											<div class="col-sm-6"><p><button  valorCampo="Clique para copiar!"class="btnCopiar"><?php echo $cliente_canal_empresa['infos'] ?></button></p></div>
										</div>
									</div>
								<?php endforeach; ?>
									
								</div>
							</div>

						</li>
					</ul>
				</div>
			</div>
		</div>

	</div>
</div>

<script>
	
function fallbackcopiarTextoParaTransferir(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);

  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copiarTextoParaTransferir(text) {
  if (!navigator.clipboard) {
    fallbackcopiarTextoParaTransferir(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}

$( ".btnCopiar" ).click(function(e) {
  let valor = $( this ).text();

  $( this ).attr("valorCampo","Texto copiado!");
  setTimeout(function(){  $(  ".btnCopiar"  ).attr("valorCampo","Clique para copiar!"); }, 1000);
  copiarTextoParaTransferir(valor);
});



</script>
<?php
get_footer();
