<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Portal_Handgran
 */

get_header();
?>

		<div class="pg-clientes pg">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<aside>
						<h2>Tipo de cliente</h2>
						<ul>
							<?php 

								//DEFINE A TAXONOMIA 
								$taxonomia = "categoriaCliente";

								// LISTA AS CATEGORIAS CLIENTES
		                        $categoriaCliente = get_terms( $taxonomia, array(
		                            'orderby'    => 'count',
		                            'hide_empty' => 0,
		                            'parent'     => 0
		                        ));  

		                        foreach ($categoriaCliente as $categoriaCliente):

							?>
							<li>
								<a href="<?php echo get_category_link($categoriaCliente->term_id); ?>"><?php echo $nome = $categoriaCliente->name; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</aside>
				</div>
				<div class="col-sm-9">
					<div class="listaClientes">
						<h2>Resultado de sua pesquisa:</h2>
						<ul>
							<?php while ( have_posts() ) : the_post(); ?>
							<li>
								<a href="<?php echo get_permalink() ?>">
									<span>Ver cliente</span>	
									<h2><?php echo get_the_title(); ?></h2>
									<figure>
										<?php 
											$urlLogo = rwmb_meta('Handgran_cliente_logo_empresa');
											foreach ($urlLogo as $urlLogo):
												$urlLogo = $urlLogo['full_url'];
										?>
										<img src="<?php echo $urlLogo  ?>" alt="<?php echo get_the_title(); ?>">
										<?php endforeach; ?>
									</figure>

								</a>
							</li>

							<?php endwhile; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
