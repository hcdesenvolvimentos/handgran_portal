<?php
/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Portal_Handgran
 */
get_header();
?>
<div class="pg-adminInicial">
	<div class="container">
		<ul>
			<li>
				<a href="<?php echo  get_admin_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/gears.svg">
					<span>Painel Admin</span>
				</a>
			</li>
			<li>
				<a href="">
					<img src="<?php echo get_template_directory_uri(); ?>/img/office-material.svg">
					<span>wikihandrgan</span>
				</a>
			</li>
			<li>
				<a href="<?php echo home_url('/clientes/'); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/003-team.svg">
					<span>Clientes</span>
				</a>
			</li>
			<li>
				<a href="<?php echo home_url('/cadastro-cliente/'); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/team.svg">
					<span>Cadastro de Funcionario</span>
				</a>
			</li>
			<li>
				<a href="<?php echo home_url('/cadastro-cliente/'); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/add-cliente.svg">
					<span>Adicionar novo cliente</span>
				</a>
			</li>
			<li>
				<a href="<?php echo home_url('/cadastro-cliente/'); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/002-adwords.svg">
					<span>Cadastro SOP - Google ADS</span>
				</a>
			</li>
			<li>
				<a href="<?php echo home_url('/cadastro-cliente/'); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/img/001-graph.svg">
					<span>Cadastro SOP - Inbound Marketing</span>
				</a>
			</li>

		</ul>
	</div>
</div>
<?php get_footer();