<?php
/**
 * Template Name: Página Cadastro Cliente
 * Description: Página Cadastro Cliente
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Portal_Handgran
 */
get_header();

	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		
		if(isset($_POST['cadastroCliente'])){

			// PEGANDO INFORMAÇÕES  FORMULÁRIO DE CADASTRO DE CLIENTES
			$nomeFantasia =  $_POST['nomeFantasia'];		
			$razaoSocial =  $_POST['razaoSocial'];		
			$cnpjEmpresa =  $_POST['cnpjEmpresa'];		
			$inscricaoEstadual =  $_POST['inscricaoEstadual'];		
			$inscricaoMunicipal =  $_POST['inscricaoMunicipal'];		
			$emailContato =  $_POST['emailContato'];		
			$telefoneContato =  $_POST['telefoneContato'];	


			// PEGANDO INFORMAÇÕES  FORMULÁRIO DE Responsável
			$nomeResponsavel =  $_POST['nomeResponsavel'];		
			$celularResponsavel =  $_POST['celularResponsavel'];		
			$cpfResponsavel =  $_POST['cpfResponsavel'];		
			$emailResponsavel =  $_POST['emailResponsavel'];		

			// PEGANDO INFORMAÇÕES  FORMULÁRIO DE Endereço
			$rua =  $_POST['rua'];		
			$numero =  $_POST['numero'];		
			$bairro =  $_POST['bairro'];		
			$cidade =  $_POST['cidade'];		
			$complemento =  $_POST['complemento'];		
			$cep =  $_POST['cep'];		

			//ARRAY PARA CADASTRAR CLENTES
			$cadastroCliente = array(
		        'post_title'    => $nomeFantasia,
		        'post_content'  => '',
		        'post_status'   => 'draft',
		        'post_type' 	=> 'cliente'
		    );

	    	$clinteID = wp_insert_post($cadastroCliente);

	    	//INSERT METABOXES FORMULÁRIO DE CADASTRO DE CLIENTES
		    add_post_meta($clinteID, 'Handgran_cliente_Fantasia_empresa', $nomeFantasia, true);
		    add_post_meta($clinteID, 'Handgran_cliente_RazaoSocial_empresa', $razaoSocial, true);
		    add_post_meta($clinteID, 'Handgran_cliente_cnpj_empresa', $cnpjEmpresa, true);
		    add_post_meta($clinteID, 'Handgran_cliente_inscricaoEstadual_empresa', $inscricaoEstadual, true);
		    add_post_meta($clinteID, 'Handgran_cliente_inscricaoMunicipal_empresa', $inscricaoMunicipal, true);
		    add_post_meta($clinteID, 'Handgran_cliente_inscricaoMunicipal_empresa', $inscricaoMunicipal, true);
		    add_post_meta($clinteID, 'Handgran_cliente_email_contato_empresa', $emailContato, true);
		    add_post_meta($clinteID, 'Handgran_cliente_numero_contato_empresa', $telefoneContato, true);

		    //INSERT METABOXES FORMULÁRIO DE Responsável
		    add_post_meta($clinteID, 'Handgran_cliente_responsavel_empresa', $nomeResponsavel, true);
		    add_post_meta($clinteID, 'Handgran_cliente_Celular_empresa', $celularResponsavel, true);
		    add_post_meta($clinteID, 'Handgran_cliente_CPF_empresa', $cpfResponsavel, true);
		    add_post_meta($clinteID, 'Handgran_cliente_Email_empresa', $emailResponsavel, true);

		     //INSERT METABOXES FORMULÁRIO DE Endereço
		    add_post_meta($clinteID, 'Handgran_cliente_rua_empresa', $rua, true);
		    add_post_meta($clinteID, 'Handgran_cliente_numero_empresa', $numero, true);
		    add_post_meta($clinteID, 'Handgran_cliente_bairro_empresa', $bairro, true);
		    add_post_meta($clinteID, 'Handgran_cliente_cidade_empresa', $cidade, true);
		    add_post_meta($clinteID, 'Handgran_cliente_complemento_empresa', $complemento, true);
		    add_post_meta($clinteID, 'Handgran_cliente_CEP_empresa', $cep, true);
		
			move_uploaded_file($_FILES["comprovanteAguaLuz"]["tmp_name"],WP_CONTENT_DIR .'/uploads/clientes/'.basename($_FILES['comprovanteAguaLuz']['name']));

			$arquivoNome  = WP_CONTENT_DIR .'/uploads/clientes/' . basename($_FILES['comprovanteAguaLuz']['name']);
			$arquivoTipo  = wp_check_filetype( basename( $arquivoNome ), null );
			$arquivoTipos = array('png','jpg','jpeg','gif');


			if(in_array($arquivoTipo['ext'], $arquivoTipos)){

				require_once( ABSPATH . 'wp-admin/includes/image.php' );

				$diretorioUploads = wp_upload_dir();

				$arquivoDetalhes = array(
					'guid'           => $diretorioUploads['url'] . '/' . basename( $arquivoNome ), 
					'post_mime_type' => $arquivoTipo['type'],
					'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $arquivoNome ) ),
					'post_content'   => '',
					'post_status'    => 'inherit'
				);

				$arquivoCarregadoId 	= wp_insert_attachment($arquivoDetalhes, $arquivoNome, $clinteID);
				$arquivoCarregadoMeta	= wp_generate_attachment_metadata($arquivoCarregadoId,$arquivoNome);

				wp_update_attachment_metadata($arquivoCarregadoId, $arquivoCarregadoMeta);

				add_post_meta($clinteID, 'Handgran_cliente_logo_empresa', $arquivoCarregadoId, true);

			}

		    if($clinteID > 0){ $cadastroRealizado = true; }

		}
	}
?>

<div class="pg pg-form">
	
	<div class="areaFormulario">
	
		<form  method="post" enctype="multipart/form-data">

			<h2>Formulário de cadastro de Cliente</h2>
			<div class="sombra">

				<section class="sessaoCadastroPessoal">
					<h6 class="hidden">Formulário de cadastro de Cliente</h6>

					<div class="areaForm">
						<div class="row">
							<div class="col-sm-6">
								
								<label for="" class="hidden">Nome Fantasia</label>
								<input required  type="text" placeholder="Nome Fantasia" id="nomeFantasia" name="nomeFantasia">

								<label for="" class="hidden">Razão social</label>
								<input required  type="text" placeholder="Razão social" id="razaoSocial" name="razaoSocial">

							</div>
							<div class="col-sm-6">
								
								<label for="" class="hidden">CNPJ</label>
								<input required  type="text" placeholder="CNPJ" id="cnpjEmpresa" name="cnpjEmpresa">

								<label for="" class="hidden">Inscrição estadual</label>
								<input required  type="text" placeholder="Inscrição estadual" id="inscricaoEstadual" name="inscricaoEstadual">

							</div>
							<div class="col-sm-6">
								

								<label for="" class="hidden">Telefone para Contato</label>
								<input required  type="text" placeholder="Telefone para Contato"  id="telefoneContato" name="telefoneContato">
							

							</div>
							<div class="col-sm-6">

								<label for="" class="hidden">Inscrição municipal</label>
								<input required  type="text" placeholder="Inscrição municipal" id="inscricaoMunicipal" name="inscricaoMunicipal">

							</div>

							<div class="col-md-12">
								<label for="" class="hidden">E-mail para contato</label>
								<input required  type="text" placeholder="E-mail para contato"  id="emailContato" name="emailContato">
							</div>
						</div>

					</div>
				</section>

			</div>

			<h2>Responsável</h2>
			<div class="sombra">
				<section class="sessaoCadastroPessoal">
					<h6 class="hidden">Responsável</h6>

					<div class="areaForm">
						<div class="row">
							<div class="col-sm-6">
								
								<label for="" class="hidden">Nome Completo Responsável</label>
								<input required  type="text" placeholder="Nome Completo Responsável" name="nomeResponsavel" id="nomeResponsavel">

							</div>
							<div class="col-sm-6">
								
								<label for="" class="hidden">Celular Responsável</label>
								<input required  type="text" placeholder="Celular Responsável" name="celularResponsavel" id="celularResponsavel">

							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								
								<label for="" class="hidden">CPF Responsável</label>
								<input required  type="text" placeholder="CPF Responsável" name="cpfResponsavel" id="cpfResponsavel">

							</div>
							<div class="col-sm-6">
								
								<label for="" class="hidden">E-mail</label>
								<input required  type="text" placeholder="E-mail" name="emailResponsavel" id="emailResponsavel">

							</div>
						</div>
						
					</div>
				</section>
			</div>

			<h2>Endereço</h2>
			<div class="sombra">
				<section class="sessaoCadastroPessoal">
					<h6 class="hidden">Endereço</h6>

					<div class="areaForm">
						<div class="row">
							<div class="col-sm-6">
								
								<label for="" class="hidden">Rua</label>
								<input required  type="text" placeholder="Rua" id="rua" name="rua">

							</div>
							<div class="col-sm-6">
								
								<label for="" class="hidden">Numero</label>
								<input required  type="text" placeholder="Numero" id="numero" name="numero">

							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								
								<label for="" class="hidden">Bairro</label>
								<input required  type="text" placeholder="Bairro" id="bairro" name="bairro">

							</div>
							<div class="col-sm-6">
								
								<label for="" class="hidden">Cidade</label>
								<input required  type="text" placeholder="Cidade" id="cidade" name="cidade">

							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								
								<label for="" class="hidden">Complemento</label>
								<input required  type="text" placeholder="Complemento" id="complemento" name="complemento">

							</div>
							<div class="col-sm-6">
								
								<label for="" class="hidden">CEP</label>
								<input required  type="text" placeholder="CEP" id="cep" name="cep">

							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								
								<label for="uploadArquivo"> Comprovante de residencia (agua ou  luz ou telefone)
									<input required  type="file" id="comprovanteAguaLuz" name="comprovanteAguaLuz">
								</label>

							</div>
						</div>
					</div>
				</section>
			</div>

			<input type="hidden" name="cadastroCliente" value="1">
			<input type="submit" value="Enviar">
		</form>

	</div>
</div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
 <script>
             $(function(){
                    $("#form_contato").validate();
             });
       </script>

<?php if ($cadastroRealizado): ?>
<div class="modalSucess" style="background: green">Sucesso</div>
<?php else: ?>
<div class="modalSucess" style="background: red">não deu</div>
<?php endif; ?>
<?php get_footer();