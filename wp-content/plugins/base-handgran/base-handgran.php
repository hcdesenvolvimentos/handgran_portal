<?php

/**
 * Plugin Name: Base Handgran
 * Description: Controle base do tema Handgran.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseHandgran() {

		// TIPOS DE CONTEÚDO
		conteudosHandgran();

		taxonomiaHandgran();

		metaboxesHandgran();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosHandgran (){

		// TIPOS DE CONTEÚDO
		tipoClientes();


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE CLIENTES
	function tipoClientes() {

		$rotuloClientes = array(
								'name'               => 'Clientes',
								'singular_name'      => 'cliente',
								'menu_name'          => 'Clientes',
								'name_admin_bar'     => 'Clientes',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo cliente',
								'new_item'           => 'Novo cliente',
								'edit_item'          => 'Editar cliente',
								'view_item'          => 'Ver cliente',
								'all_items'          => 'Todos os clientes',
								'search_items'       => 'Buscar cliente',
								'parent_item_colon'  => 'Dos clientes',
								'not_found'          => 'Nenhum cliente cadastrado.',
								'not_found_in_trash' => 'Nenhum cliente na lixeira.'
							);

		$argClientes 	= array(
								'labels'             => $rotuloClientes,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-groups',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'clientes' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor', 'thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('cliente', $argClientes);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaHandgran () {		
		taxonomiaCategoriaClientes();
	}
		// TAXONOMIA DE CLIENTES
		function taxonomiaCategoriaClientes() {

			$rotulosCategoriaClientes = array(
				'name'              => 'Categorias de cliente',
				'singular_name'     => 'Categoria de cliente',
				'search_items'      => 'Buscar categorias de cliente',
				'all_items'         => 'Todas as categorias de cliente',
				'parent_item'       => 'Categoria de cliente pai',
				'parent_item_colon' => 'Categoria de cliente pai:',
				'edit_item'         => 'Editar categoria de cliente',
				'update_item'       => 'Atualizar categoria de cliente',
				'add_new_item'      => 'Nova categoria de cliente',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de clientes',
				);

			$argsCategoriaClientes 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaClientes,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-clientes' ),
				);

			register_taxonomy( 'categoriaCliente', array( 'cliente' ), $argsCategoriaClientes );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesHandgran(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'Handgran_';

			// METABOX DE CLIENTES Cadastro de Cliente
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxSopGoogleAds',
				'title'			=> 'Cadastro de Cliente',
				'pages' 		=> array( 'cliente' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Logo',
						'id'    => "{$prefix}cliente_logo_empresa",
						'desc'  => 'Logo',
						'type'  		   => 'image_advanced',
						'max_file_uploads' => 1
					),

					array(
						'name'  => 'Fantasia',
						'id'    => "{$prefix}cliente_Fantasia_empresa",
						'desc'  => 'Fantasia',
						'type'  => 'text',
					),	

					array(
						'name'  => 'Razão social',
						'id'    => "{$prefix}cliente_RazaoSocial_empresa",
						'desc'  => 'Razão social',
						'type'  => 'text',
					),

					array(
						'name'  => 'CNPJ',
						'id'    => "{$prefix}cliente_cnpj_empresa",
						'desc'  => 'CNPJ',
						'type'  => 'text',
					),

					array(
						'name'  => 'Inscrição estadual',
						'id'    => "{$prefix}cliente_inscricaoEstadual_empresa",
						'desc'  => 'Inscrição estadual',
						'type'  => 'text',
					),

					array(
						'name'  => 'Inscrição municipal',
						'id'    => "{$prefix}cliente_inscricaoMunicipal_empresa",
						'desc'  => 'Inscrição municipal',
						'type'  => 'text',
					),

					array(
						'name'  => 'E-mail para contato',
						'id'    => "{$prefix}cliente_email_contato_empresa",
						'desc'  => 'E-mail para contato',
						'type'  => 'text',
					),	

					array(
						'name'  => 'Telefone para Contato',
						'id'    => "{$prefix}cliente_numero_contato_empresa",
						'desc'  => 'Telefone para Contato',
						'type'  => 'text',
					),
					
				),
			);

			// METABOX DE CLIENTES Endereço
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxEndereco',
				'title'			=> 'Cadastro de Cliente - Endereço',
				'pages' 		=> array( 'cliente' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Rua',
						'id'    => "{$prefix}cliente_rua_empresa",
						'desc'  => 'Rua',
						'type'  => 'text',
					),	

					array(
						'name'  => 'Numero',
						'id'    => "{$prefix}cliente_numero_empresa",
						'desc'  => 'Numero',
						'type'  => 'text',
					),	

					array(
						'name'  => 'Bairro',
						'id'    => "{$prefix}cliente_bairro_empresa",
						'desc'  => 'Bairro',
						'type'  => 'text',
					),	

					array(
						'name'  => 'Cidade',
						'id'    => "{$prefix}cliente_cidade_empresa",
						'desc'  => 'Cidade',
						'type'  => 'text',
					),

					array(
						'name'  => 'Complemento',
						'id'    => "{$prefix}cliente_complemento_empresa",
						'desc'  => 'Complemento',
						'type'  => 'text',
					),

					array(
						'name'  => 'CEP',
						'id'    => "{$prefix}cliente_CEP_empresa",
						'desc'  => 'CEP',
						'type'  => 'text',
					),


				),
			);

			// METABOX DE CLIENTES Responsável
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxResponsavel',
				'title'			=> 'Cadastro de Cliente - Responsável',
				'pages' 		=> array( 'cliente' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Nome Completo - Responsável',
						'id'    => "{$prefix}cliente_responsavel_empresa",
						'desc'  => 'Nome Completo',
						'type'  => 'text',
					),	

					array(
						'name'  => 'Email - Responsável',
						'id'    => "{$prefix}cliente_Email_empresa",
						'desc'  => 'Email',
						'type'  => 'text',
					),	

					array(
						'name'  => 'Celular - Responsável',
						'id'    => "{$prefix}cliente_Celular_empresa",
						'desc'  => 'Celular',
						'type'  => 'text',
					),

					array(
						'name'  => 'Contatos - Responsável',
						'id'    => "{$prefix}cliente_contatos_empresa",
						'desc'  => 'Nome | Telefone',
						'placeholder'  => 'Anderson | (41) 997117123',
						'type'  => 'text',
						'clone'  => true,
					),

					array(
						'name'  => 'CPF - Responsável',
						'id'    => "{$prefix}cliente_CPF_empresa",
						'desc'  => 'CPF',
						'type'  => 'text',
					),

					array(
						'name'  => 'Observação - Responsável',
						'id'    => "{$prefix}cliente_observacao_empresa",
						'desc'  => 'Observação',
						'type'  => 'textarea',
					),

					
					
				),
			);

			// METABOX DE CLIENTES SERVIÇO
			$metaboxes[] = array(
				'id'			=> 'detalhesMetaboxCanaisAcesso',
				'title'			=> 'Cadastro de Cliente - Canais de acesso',
				'pages' 		=> array( 'cliente' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
					    'id'      => 'cliente_canal_empresa',
					    'name'    => 'Canais de acesso',
					    'type'    => 'fieldset_text',

					    // Options: array of key => Label for text boxes
					    // Note: key is used as key of array of values stored in the database
					    'options' => array(
					        'canal'   => 'Canal',
					        'url'     => 'Url',
					        'login'   => 'Login',
					        'senha'   => 'Senha',
					        'infos'   => 'Infos',
					    ),



					    // Is field cloneable?
					    'clone' => true,
					),

				),
			);

			

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesHandgran(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerHandgran(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseHandgran');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseHandgran();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );


	remove_action('welcome_panel','wp_welcome_panel');
	add_action('welcome_panel','my_welcome_panel');


	function my_welcome_panel(){
		include'CSS/main.css';
	?>

		<div class="welcome-panel-content">
			<h3>Seja Bem vindo ao painel!</h3>
			<p>Navegue em seu painel:</p>
			<figure>
				<img src="http://localhost/projetos/handgran_portal/wp-content/uploads/2019/05/logoMarca.png">
			</figure>
		</div>

	<?php
	}