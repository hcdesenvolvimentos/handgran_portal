<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_handgran_portal' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'au?q|Q)v:]iLpPUBR-g_9?JMUq!4_^u03dJkdf-,Eod0[w3Ip3|I*dS@en:h%0np' );
define( 'SECURE_AUTH_KEY',  'bez0lVM< .n9mYn23lC[$zi&0nueCQ *K3y+1c^8G5x4>`$]>{M%<+RCbE8v$Z%O' );
define( 'LOGGED_IN_KEY',    ')MJ@L:jt3C,@ad9T3w1. MdEQO]FIfcg~ 4R=vdrCJu~g?gdAHTsldW ZgL$tv$v' );
define( 'NONCE_KEY',        'do!AC%zHeJKdP(Vm6F2c1ZIv(2NH*HnAxOf.Uh+%b][#%rX1V_K!Psi(ta!8&KbF' );
define( 'AUTH_SALT',        '<)XB}Owd#k-Db_-9CIrI[0_!9@Clpt~Rcm[K6:i]w1HfsZ@&W<fe_rRN,5n!_(C-' );
define( 'SECURE_AUTH_SALT', '2CIvUDbmUBAx8t497!cYm(1O} zmWg.[4vQA$`3,2aX-ji5z1]lQ:5t,<U0QJ:ND' );
define( 'LOGGED_IN_SALT',   'Ap<~;Lm?&2=dQKsgiA[E5Jh|jbu#U8e21Xr#(xx84]^  *N(VLqTXYOXBR< Shtf' );
define( 'NONCE_SALT',       'EemC=A-0bha3kbauH6ip.9SvT!MD~o_NTY;}#MUMR;Icr8ScGL=61Gn[R6Hy9kxe' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'hp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
