-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 15-Jul-2019 às 14:49
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_handgran_portal`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_commentmeta`
--

DROP TABLE IF EXISTS `hp_commentmeta`;
CREATE TABLE IF NOT EXISTS `hp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_comments`
--

DROP TABLE IF EXISTS `hp_comments`;
CREATE TABLE IF NOT EXISTS `hp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_comments`
--

INSERT INTO `hp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-05-08 10:30:53', '2019-05-08 13:30:53', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_links`
--

DROP TABLE IF EXISTS `hp_links`;
CREATE TABLE IF NOT EXISTS `hp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_options`
--

DROP TABLE IF EXISTS `hp_options`;
CREATE TABLE IF NOT EXISTS `hp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=306 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_options`
--

INSERT INTO `hp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/handgran_portal', 'yes'),
(2, 'home', 'http://localhost/projetos/handgran_portal', 'yes'),
(3, 'blogname', 'Portal Handgran', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'hudson@handgran.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:115:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"clientes/?$\";s:27:\"index.php?post_type=cliente\";s:41:\"clientes/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=cliente&feed=$matches[1]\";s:36:\"clientes/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=cliente&feed=$matches[1]\";s:28:\"clientes/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=cliente&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"clientes/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"clientes/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"clientes/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"clientes/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"clientes/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"clientes/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"clientes/([^/]+)/embed/?$\";s:40:\"index.php?cliente=$matches[1]&embed=true\";s:29:\"clientes/([^/]+)/trackback/?$\";s:34:\"index.php?cliente=$matches[1]&tb=1\";s:49:\"clientes/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?cliente=$matches[1]&feed=$matches[2]\";s:44:\"clientes/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?cliente=$matches[1]&feed=$matches[2]\";s:37:\"clientes/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?cliente=$matches[1]&paged=$matches[2]\";s:44:\"clientes/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?cliente=$matches[1]&cpage=$matches[2]\";s:33:\"clientes/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?cliente=$matches[1]&page=$matches[2]\";s:25:\"clientes/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"clientes/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"clientes/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"clientes/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"clientes/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"clientes/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:59:\"categoria-clientes/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?categoriaCliente=$matches[1]&feed=$matches[2]\";s:54:\"categoria-clientes/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?categoriaCliente=$matches[1]&feed=$matches[2]\";s:35:\"categoria-clientes/([^/]+)/embed/?$\";s:49:\"index.php?categoriaCliente=$matches[1]&embed=true\";s:47:\"categoria-clientes/([^/]+)/page/?([0-9]{1,})/?$\";s:56:\"index.php?categoriaCliente=$matches[1]&paged=$matches[2]\";s:29:\"categoria-clientes/([^/]+)/?$\";s:38:\"index.php?categoriaCliente=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=22&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(288, 'recovery_mode_email_last_sent', '1557951842', 'yes'),
(304, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1563202082;s:7:\"checked\";a:1:{s:15:\"portal_handgran\";s:0:\"\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(205, 'category_children', 'a:0:{}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:31:\"base-handgran/base-handgran.php\";i:1;s:54:\"login-logo-editor-by-oizuled/login-logo-by-oizuled.php\";i:2;s:21:\"meta-box/meta-box.php\";i:4;s:33:\"wp-force-login/wp-force-login.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'portal_handgran', 'yes'),
(41, 'stylesheet', 'portal_handgran', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:27:\"redirection/redirection.php\";a:2:{i:0;s:17:\"Redirection_Admin\";i:1;s:16:\"plugin_uninstall\";}}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '22', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'hp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(157, 'widget_theme-my-login', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(158, '_tml_installed_at', '1557316676', 'no'),
(159, '_tml_updated_at', '1557316676', 'no'),
(160, '_tml_version', '7.0.13', 'no'),
(103, 'cron', 'a:6:{i:1563204654;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1563240654;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1563283853;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1563283886;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1563283888;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1557327057;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(300, '_site_transient_timeout_theme_roots', '1563203879', 'no'),
(301, '_site_transient_theme_roots', 'a:1:{s:15:\"portal_handgran\";s:7:\"/themes\";}', 'no'),
(202, 'redirection_options', 'a:20:{s:7:\"support\";b:0;s:5:\"token\";s:32:\"dd8db747fb343466d868854041e44c1a\";s:12:\"monitor_post\";i:0;s:13:\"monitor_types\";a:0:{}s:19:\"associated_redirect\";s:0:\"\";s:11:\"auto_target\";s:0:\"\";s:15:\"expire_redirect\";i:-1;s:10:\"expire_404\";i:-1;s:7:\"modules\";a:0:{}s:10:\"newsletter\";b:0;s:14:\"redirect_cache\";i:1;s:10:\"ip_logging\";i:0;s:13:\"last_group_id\";i:1;s:8:\"rest_api\";i:0;s:5:\"https\";b:0;s:8:\"database\";s:3:\"4.1\";s:10:\"flag_query\";s:5:\"exact\";s:9:\"flag_case\";b:0;s:13:\"flag_trailing\";b:0;s:10:\"flag_regex\";b:0;}', 'yes'),
(133, 'can_compress_scripts', '1', 'no'),
(142, 'recently_activated', 'a:2:{s:27:\"redirection/redirection.php\";i:1557854907;s:33:\"theme-my-login/theme-my-login.php\";i:1557788009;}', 'yes'),
(305, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1563202083;s:7:\"checked\";a:6:{s:31:\"base-handgran/base-handgran.php\";s:3:\"0.1\";s:33:\"wp-force-login/wp-force-login.php\";s:3:\"5.3\";s:54:\"login-logo-editor-by-oizuled/login-logo-by-oizuled.php\";s:5:\"1.2.2\";s:21:\"meta-box/meta-box.php\";s:6:\"4.18.0\";s:27:\"redirection/redirection.php\";s:5:\"4.2.3\";s:33:\"theme-my-login/theme-my-login.php\";s:6:\"7.0.13\";}s:8:\"response\";a:4:{s:54:\"login-logo-editor-by-oizuled/login-logo-by-oizuled.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:42:\"w.org/plugins/login-logo-editor-by-oizuled\";s:4:\"slug\";s:28:\"login-logo-editor-by-oizuled\";s:6:\"plugin\";s:54:\"login-logo-editor-by-oizuled/login-logo-by-oizuled.php\";s:11:\"new_version\";s:5:\"1.3.1\";s:3:\"url\";s:59:\"https://wordpress.org/plugins/login-logo-editor-by-oizuled/\";s:7:\"package\";s:77:\"https://downloads.wordpress.org/plugin/login-logo-editor-by-oizuled.1.3.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/login-logo-editor-by-oizuled/assets/icon-256x256.png?rev=1004695\";s:2:\"1x\";s:81:\"https://ps.w.org/login-logo-editor-by-oizuled/assets/icon-128x128.png?rev=1004695\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:82:\"https://ps.w.org/login-logo-editor-by-oizuled/assets/banner-772x250.png?rev=819309\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:115:\"<ul>\n<li>Fix: Issue where relative URLs used for login logos were displaying PHP warnings in some cases.</li>\n</ul>\";s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.18.4\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.18.4.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"redirection/redirection.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/redirection\";s:4:\"slug\";s:11:\"redirection\";s:6:\"plugin\";s:27:\"redirection/redirection.php\";s:11:\"new_version\";s:5:\"4.3.1\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/redirection/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/redirection.4.3.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/redirection/assets/icon-256x256.jpg?rev=983639\";s:2:\"1x\";s:63:\"https://ps.w.org/redirection/assets/icon-128x128.jpg?rev=983640\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/redirection/assets/banner-1544x500.jpg?rev=983641\";s:2:\"1x\";s:65:\"https://ps.w.org/redirection/assets/banner-772x250.jpg?rev=983642\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"theme-my-login/theme-my-login.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/theme-my-login\";s:4:\"slug\";s:14:\"theme-my-login\";s:6:\"plugin\";s:33:\"theme-my-login/theme-my-login.php\";s:11:\"new_version\";s:6:\"7.0.14\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/theme-my-login/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/theme-my-login.7.0.14.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/theme-my-login/assets/icon-256x256.png?rev=1891232\";s:2:\"1x\";s:67:\"https://ps.w.org/theme-my-login/assets/icon-128x128.png?rev=1891232\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/theme-my-login/assets/banner-1544x500.png?rev=1891232\";s:2:\"1x\";s:69:\"https://ps.w.org/theme-my-login/assets/banner-772x250.png?rev=1891232\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.18.0\";s:7:\"updated\";s:19:\"2018-12-05 02:06:02\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/meta-box/4.18.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"redirection\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"4.2.3\";s:7:\"updated\";s:19:\"2019-04-15 01:13:07\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/redirection/4.2.3/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"theme-my-login\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"6.4.12\";s:7:\"updated\";s:19:\"2015-11-08 01:08:52\";s:7:\"package\";s:82:\"https://downloads.wordpress.org/translation/plugin/theme-my-login/6.4.12/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:1:{s:33:\"wp-force-login/wp-force-login.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/wp-force-login\";s:4:\"slug\";s:14:\"wp-force-login\";s:6:\"plugin\";s:33:\"wp-force-login/wp-force-login.php\";s:11:\"new_version\";s:3:\"5.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-force-login/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-force-login.5.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/wp-force-login/assets/icon-256x256.png?rev=1768800\";s:2:\"1x\";s:59:\"https://ps.w.org/wp-force-login/assets/icon.svg?rev=1904031\";s:3:\"svg\";s:59:\"https://ps.w.org/wp-force-login/assets/icon.svg?rev=1904031\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wp-force-login/assets/banner-1544x500.png?rev=1768800\";s:2:\"1x\";s:69:\"https://ps.w.org/wp-force-login/assets/banner-772x250.png?rev=1768800\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(208, 'oizuled-login-logo-img', 'http://localhost/projetos/handgran_portal/wp-content/uploads/2019/05/logohandgran.png', 'yes'),
(209, 'oizuled-login-logo-link', 'http://wordpress.org', 'yes'),
(210, 'oizuled-login-logo-title', 'Powered by WordPress', 'yes'),
(211, 'oizuled-login-logo-css', '.login h1 a {\r\n    background-image: url(../images/w-logo-blue.png?ver=20131202);\r\n    background-image: none,url(../images/wordpress-logo.svg?ver=20131107);\r\n    background-size: 84px;\r\n    background-position: center top;\r\n    background-repeat: no-repeat;\r\n    color: #444;\r\n    height: 84px;\r\n    font-size: 20px;\r\n    font-weight: 400;\r\n    line-height: 1.3em;\r\n    margin: 0 auto 25px;\r\n    padding: 0;\r\n    text-decoration: none;\r\n    width: 227px!important;\r\n    text-indent: -9999px;\r\n    outline: 0;\r\n    overflow: hidden;\r\n    display: block;\r\n    background-size: cover!important;\r\n    height: 47px!important;\r\n}', 'yes'),
(150, 'current_theme', 'Portal Handgran', 'yes'),
(151, 'theme_mods_portal_handgran', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:20;}', 'yes'),
(152, 'theme_switched', '', 'yes'),
(163, 'tml_login_type', 'default', 'yes'),
(164, 'tml_registration_type', 'default', 'yes'),
(165, 'tml_user_passwords', '', 'yes'),
(166, 'tml_auto_login', '', 'yes'),
(167, 'tml_login_slug', 'login', 'yes'),
(168, 'tml_logout_slug', 'sair', 'yes'),
(169, 'tml_register_slug', 'cadastre-se', 'yes'),
(170, 'tml_lostpassword_slug', 'senha-perdida', 'yes'),
(171, 'tml_resetpass_slug', 'redefinir-senha', 'yes'),
(303, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:4:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.2-partial-0.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:3:\"5.2\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1563202082;s:15:\"version_checked\";s:3:\"5.2\";s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.2\";s:7:\"updated\";s:19:\"2019-05-20 13:33:40\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/translation/core/5.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(278, 'categoriaCliente_children', 'a:0:{}', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_postmeta`
--

DROP TABLE IF EXISTS `hp_postmeta`;
CREATE TABLE IF NOT EXISTS `hp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_postmeta`
--

INSERT INTO `hp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(7, 19, '_wp_attached_file', '2019/05/logohandgran.png'),
(8, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:429;s:6:\"height\";i:90;s:4:\"file\";s:24:\"2019/05/logohandgran.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"logohandgran-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"logohandgran-300x63.png\";s:5:\"width\";i:300;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(9, 20, '_wp_attached_file', '2019/05/cropped-logohandgran.png'),
(10, 20, '_wp_attachment_context', 'custom-logo'),
(11, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:429;s:6:\"height\";i:90;s:4:\"file\";s:32:\"2019/05/cropped-logohandgran.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"cropped-logohandgran-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"cropped-logohandgran-300x63.png\";s:5:\"width\";i:300;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(14, 22, '_edit_lock', '1557790903:1'),
(15, 22, '_wp_page_template', 'pages/inicial.php'),
(16, 25, '_edit_last', '1'),
(17, 25, '_edit_lock', '1557952774:1'),
(18, 26, '_wp_attached_file', '2019/05/CRIS_CIA_LOGO.png'),
(19, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:265;s:6:\"height\";i:78;s:4:\"file\";s:25:\"2019/05/CRIS_CIA_LOGO.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"CRIS_CIA_LOGO-150x78.png\";s:5:\"width\";i:150;s:6:\"height\";i:78;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(70, 25, 'Handgran_cliente_logo_empresa', '26'),
(21, 25, 'Handgran_cliente_Fantasia_empresa', 'CriseCia'),
(22, 25, 'Handgran_cliente_RazaoSocial_empresa', 'CriseCia'),
(23, 25, 'Handgran_cliente_cnpj_empresa', '000000000'),
(24, 25, 'Handgran_cliente_inscricaoEstadual_empresa', '000000000'),
(25, 25, 'Handgran_cliente_inscricaoMunicipal_empresa', '000000000'),
(26, 25, 'Handgran_cliente_email_contato_empresa', 'rafaelizepon@hotmail.com'),
(27, 25, 'Handgran_cliente_numero_contato_empresa', '19 99207-8815'),
(28, 25, 'Handgran_cliente_rua_empresa', 'RUA EPHRAIN RODRIGUES ALVES'),
(29, 25, 'Handgran_cliente_numero_empresa', '416'),
(30, 25, 'Handgran_cliente_bairro_empresa', 'JARDIM CLAUDIA'),
(31, 25, 'Handgran_cliente_cidade_empresa', 'LEME'),
(32, 25, 'Handgran_cliente_complemento_empresa', 'Casa'),
(33, 25, 'Handgran_cliente_CEP_empresa', '13610720'),
(34, 25, 'Handgran_cliente_responsavel_empresa', 'Rafael Izepon'),
(35, 25, 'Handgran_cliente_Email_empresa', 'rafaelizepon@hotmail.com'),
(36, 25, 'Handgran_cliente_Celular_empresa', '19 99207-8815'),
(37, 25, 'Handgran_cliente_contatos_empresa', 'a:2:{i:0;s:31:\"Whats Atendimento|19 99207-8815\";i:1;s:56:\"Gabriel Izepon|19 99201-8842 | gabrielizepon@hotmail.com\";}'),
(38, 25, 'Handgran_cliente_CPF_empresa', '000000000'),
(39, 25, 'Handgran_cliente_observacao_empresa', 'Home destaques  (área útil 980 x 350px) <br>\r\nhttp://admin.crisecia.com.br/Hotsite/HotsiteDetails/84712 \r\n\r\nHome Lançamentos <br>\r\nhttp://admin.crisecia.com.br/Hotsite/HotsiteDetails/84711 \r\n\r\nHome os Mais vendidos: <br>\r\nhttp://admin.crisecia.com.br/Hotsite/HotsiteDetails/84713\r\n\r\nOfertas por departamento: <br>\r\nhttp://admin.crisecia.com.br/VitrineHotSite'),
(40, 25, 'cliente_canal_empresa', 'a:3:{i:0;a:5:{s:5:\"canal\";s:3:\"B2w\";s:3:\"url\";s:26:\"www.b2wmarketplace.com.br \";s:5:\"login\";s:19:\"B2W_PARCEIRO_WILLLP\";s:5:\"senha\";s:7:\"sqj8792\";s:5:\"infos\";s:41:\"API KEY: E20F677E3AB45CC2425FC1142434CB68\";}i:1;a:5:{s:5:\"canal\";s:13:\"B2w - suporte\";s:3:\"url\";s:36:\"https://suportevendasb2w.zendesk.com\";s:5:\"login\";s:22:\"willerson@handgran.com\";s:5:\"senha\";s:7:\"sqj8792\";s:5:\"infos\";s:3:\"123\";}i:2;a:5:{s:5:\"canal\";s:6:\"Criteo\";s:3:\"url\";s:28:\"https://integrate.criteo.com\";s:5:\"login\";s:20:\"marketing.criteo.com\";s:5:\"senha\";s:22:\"willerson@handgran.com\";s:5:\"infos\";s:10:\"C8abn1223!\";}}'),
(41, 29, '_edit_lock', '1557952629:1'),
(42, 29, '_edit_last', '1'),
(43, 30, '_wp_attached_file', '2019/05/logoMarca.png'),
(44, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:166;s:6:\"height\";i:61;s:4:\"file\";s:21:\"2019/05/logoMarca.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"logoMarca-150x61.png\";s:5:\"width\";i:150;s:6:\"height\";i:61;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(66, 29, 'Handgran_cliente_logo_empresa', '30'),
(46, 29, 'cliente_canal_empresa', 'a:1:{i:0;a:5:{s:5:\"canal\";s:27:\"333333333333333333333333333\";s:3:\"url\";s:27:\"333333333333333333333333333\";s:5:\"login\";s:27:\"333333333333333333333333333\";s:5:\"senha\";s:27:\"333333333333333333333333333\";s:5:\"infos\";s:27:\"333333333333333333333333333\";}}'),
(47, 31, '_edit_lock', '1557952774:1'),
(48, 31, '_edit_last', '1'),
(49, 32, '_wp_attached_file', '2019/05/logo-azul.png'),
(50, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:156;s:6:\"height\";i:32;s:4:\"file\";s:21:\"2019/05/logo-azul.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"logo-azul-150x32.png\";s:5:\"width\";i:150;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(68, 31, 'Handgran_cliente_logo_empresa', '32'),
(52, 31, 'cliente_canal_empresa', 'a:1:{i:0;a:5:{s:5:\"canal\";s:27:\"333333333333333333333333333\";s:3:\"url\";s:27:\"333333333333333333333333333\";s:5:\"login\";s:27:\"333333333333333333333333333\";s:5:\"senha\";s:27:\"333333333333333333333333333\";s:5:\"infos\";s:27:\"333333333333333333333333333\";}}'),
(57, 25, '_oembed_2d32ef55cc3631f6a7ed61850c7d8abf', '{{unknown}}'),
(65, 25, '{$prefix}cliente_canal_empresa', 'a:5:{s:5:\"canal\";s:27:\"333333333333333333333333333\";s:3:\"url\";s:27:\"333333333333333333333333333\";s:5:\"login\";s:27:\"333333333333333333333333333\";s:5:\"senha\";s:27:\"333333333333333333333333333\";s:5:\"infos\";s:27:\"333333333333333333333333333\";}'),
(67, 29, '{$prefix}cliente_canal_empresa', 'a:1:{i:0;a:5:{s:5:\"canal\";s:27:\"333333333333333333333333333\";s:3:\"url\";s:27:\"333333333333333333333333333\";s:5:\"login\";s:27:\"333333333333333333333333333\";s:5:\"senha\";s:27:\"333333333333333333333333333\";s:5:\"infos\";s:27:\"333333333333333333333333333\";}}'),
(69, 31, '{$prefix}cliente_canal_empresa', 'a:1:{i:0;a:5:{s:5:\"canal\";s:27:\"333333333333333333333333333\";s:3:\"url\";s:27:\"333333333333333333333333333\";s:5:\"login\";s:27:\"333333333333333333333333333\";s:5:\"senha\";s:27:\"333333333333333333333333333\";s:5:\"infos\";s:27:\"333333333333333333333333333\";}}'),
(71, 35, '_edit_lock', '1558361343:1'),
(72, 35, '_wp_page_template', 'pages/adicionar-cliente.php');

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_posts`
--

DROP TABLE IF EXISTS `hp_posts`;
CREATE TABLE IF NOT EXISTS `hp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_posts`
--

INSERT INTO `hp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-05-08 10:30:53', '2019-05-08 13:30:53', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2019-05-08 10:30:53', '2019-05-08 13:30:53', '', 0, 'http://localhost/projetos/handgran_portal/?p=1', 0, 'post', '', 1),
(2, 1, '2019-05-08 10:30:53', '2019-05-08 13:30:53', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/handgran_portal/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2019-05-08 10:30:53', '2019-05-08 13:30:53', '', 0, 'http://localhost/projetos/handgran_portal/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-05-08 10:30:53', '2019-05-08 13:30:53', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://localhost/projetos/handgran_portal.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2019-05-08 10:30:53', '2019-05-08 13:30:53', '', 0, 'http://localhost/projetos/handgran_portal/?page_id=3', 0, 'page', '', 0),
(33, 1, '2019-05-15 16:51:16', '2019-05-15 19:51:16', 'Concorrentes:\n\nhttps://www.brasilcowboy.com.br/ https://www.rodeowest.com.br/ https://www.salomaocountry.com.br/\n\nhttps://www.7mboots.com.br/ https://www.westernshop.com.br/ http://www.mreis.com.br\n\nhttp://www.cowboys.com.br/ http://arenamodacountry.com.br https://www.barretesao.com.br/\n\nhttp://www.ranchocountry.com.br/ https://www.lojaceleirocountry.com.br/ https://www.lojatexas.com.br\n\nhttp://www.silveradobotas.com.br/ http://armazemcountrybh.com.br/novo2/ https://www.zonacountry.com.br\n\nhttp://www.zapcountry.com.br http://www.otoro.com.br/ https://www.calamari.com.br/\n\nhttps://www.selariacampolina.com.br/ https://arizonacountry.com.br/ https://www.tuticountry.com.br/\n\nhttp://jeitocountry.com.br/ https://www.westland.com.br/ http://lojaoriginalcountry.com.br/\n\nhttp://www.cowhorse.com.br/ http://www.lcountrycorrea.com.br http://www.countrystyle.com.br/ http://www.oklahomacountry.com.br/ http://www.betterhorse.com.br/ http://biancardishop.mercadoshops.com.br/ https://www.showhorse.com.br/ http://texas10.com.br http://americantexas.com.br http://www.correariacampana.com.br https://www.indiretasbrutas.com.br/diversos http://atletasteamroping.com.br/ http://www.loja.clubedocavalo.com/ http://www.curtamais.com.br/goiania/5-lugares-para-voce-garantir-um-look-country-e-arrasar-na-pecuaria http://g1.globo.com/sao-paulo/noticia/2010/08/moda-country-de-luxo-tem-bota-de-couro-de-cobra-por-r-2-mil.html https://www.google.com.br/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=33&amp;cad=rja&amp;uact=8&amp;ved=0ahUKEwj60MTgyvjXAhWJiZAKHdVMDGY4HhAWCE4wAg&amp;url=http%3A%2F%2Fentretenimento.r7.com%2Fmoda-e-beleza%2Fnoticias%2Ffernando-sorocaba-preferem-mulheres-de-bota-e-chapeu-20120420.html%3Fquestion%3D0&amp;usg=AOvVaw1Tk01jRilQOzu-R-AoB8v0 https://tudocommoda.com/looks/look-country-inspire-se-com-x-looks-incriveis/ https://www.google.com.br/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=40&amp;cad=rja&amp;uact=8&amp;ved=0ahUKEwj60MTgyvjXAhWJiZAKHdVMDGY4HhAWCHYwCQ&amp;url=http%3A%2F%2Fwww1.folha.uol.com.br%2Ffsp%2Fribeirao%2Fri16089859.htm&amp;usg=AOvVaw1IcXGb8TwkJ6vgirpeJ1pj https://glamurama.uol.com.br/bota-e-chapeu-acessorios-obrigatorios-em-barretos/ http://2018dicas.com/moda-country-2018-roupas-botas-chapeu-fotos-2018 http://aesquina.com.br/index.php/botas-xm-especiais-xm/country-i/item/140-bota-anaconda-verniz-vermelha', 'CriseCia', '', 'inherit', 'closed', 'closed', '', '25-autosave-v1', '', '', '2019-05-15 16:51:16', '2019-05-15 19:51:16', '', 25, 'http://localhost/projetos/handgran_portal/25-autosave-v1/', 0, 'revision', '', 0),
(29, 1, '2019-05-15 13:33:32', '2019-05-15 16:33:32', '', 'ImpulseFix', '', 'publish', 'closed', 'closed', '', 'impulsefix', '', '', '2019-05-15 17:17:09', '2019-05-15 20:17:09', '', 0, 'http://localhost/projetos/handgran_portal/?post_type=cliente&#038;p=29', 0, 'cliente', '', 0),
(30, 1, '2019-05-15 13:33:28', '2019-05-15 16:33:28', '', 'logoMarca', '', 'inherit', 'open', 'closed', '', 'logomarca', '', '', '2019-05-15 13:33:28', '2019-05-15 16:33:28', '', 0, 'http://localhost/projetos/handgran_portal/wp-content/uploads/2019/05/logoMarca.png', 0, 'attachment', 'image/png', 0),
(31, 1, '2019-05-15 13:34:31', '2019-05-15 16:34:31', '', 'Decisão', '', 'publish', 'closed', 'closed', '', 'decisao', '', '', '2019-05-15 17:17:19', '2019-05-15 20:17:19', '', 0, 'http://localhost/projetos/handgran_portal/?post_type=cliente&#038;p=31', 0, 'cliente', '', 0),
(32, 1, '2019-05-15 13:34:27', '2019-05-15 16:34:27', '', 'logo-azul', '', 'inherit', 'open', 'closed', '', 'logo-azul', '', '', '2019-05-15 13:34:27', '2019-05-15 16:34:27', '', 0, 'http://localhost/projetos/handgran_portal/wp-content/uploads/2019/05/logo-azul.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2019-05-20 10:45:45', '2019-05-20 13:45:45', '', 'Cadastro Cliente', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2019-05-20 10:45:45', '2019-05-20 13:45:45', '', 35, 'http://localhost/projetos/handgran_portal/35-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2019-05-20 10:45:45', '2019-05-20 13:45:45', '', 'Cadastro Cliente', '', 'publish', 'closed', 'closed', '', 'cadastro-cliente', '', '', '2019-05-20 10:46:18', '2019-05-20 13:46:18', '', 0, 'http://localhost/projetos/handgran_portal/?page_id=35', 0, 'page', '', 0),
(19, 1, '2019-05-13 20:08:33', '2019-05-13 23:08:33', '', 'logohandgran', '', 'inherit', 'open', 'closed', '', 'logohandgran', '', '', '2019-05-13 20:08:33', '2019-05-13 23:08:33', '', 0, 'http://localhost/projetos/handgran_portal/wp-content/uploads/2019/05/logohandgran.png', 0, 'attachment', 'image/png', 0),
(20, 1, '2019-05-13 20:08:44', '2019-05-13 23:08:44', 'http://localhost/projetos/handgran_portal/wp-content/uploads/2019/05/cropped-logohandgran.png', 'cropped-logohandgran.png', '', 'inherit', 'open', 'closed', '', 'cropped-logohandgran-png', '', '', '2019-05-13 20:08:44', '2019-05-13 23:08:44', '', 0, 'http://localhost/projetos/handgran_portal/wp-content/uploads/2019/05/cropped-logohandgran.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2019-05-13 20:18:50', '2019-05-13 23:18:50', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-05-13 20:30:26', '2019-05-13 23:30:26', '', 0, 'http://localhost/projetos/handgran_portal/?page_id=22', 0, 'page', '', 0),
(23, 1, '2019-05-13 20:18:50', '2019-05-13 23:18:50', '', 'Home', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2019-05-13 20:18:50', '2019-05-13 23:18:50', '', 22, 'http://localhost/projetos/handgran_portal/2019/05/13/22-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2019-05-14 17:18:39', '2019-05-14 20:18:39', 'Concorrentes:\r\n\r\nhttps://www.brasilcowboy.com.br/ https://www.rodeowest.com.br/ https://www.salomaocountry.com.br/\r\n\r\nhttps://www.7mboots.com.br/ https://www.westernshop.com.br/ http://www.mreis.com.br\r\n\r\nhttp://www.cowboys.com.br/ http://arenamodacountry.com.br https://www.barretesao.com.br/\r\n\r\nhttp://www.ranchocountry.com.br/ https://www.lojaceleirocountry.com.br/ https://www.lojatexas.com.br\r\n\r\nhttp://www.silveradobotas.com.br/ http://armazemcountrybh.com.br/novo2/ https://www.zonacountry.com.br\r\n\r\nhttp://www.zapcountry.com.br http://www.otoro.com.br/ https://www.calamari.com.br/\r\n\r\nhttps://www.selariacampolina.com.br/ https://arizonacountry.com.br/ https://www.tuticountry.com.br/\r\n\r\nhttp://jeitocountry.com.br/ https://www.westland.com.br/ http://lojaoriginalcountry.com.br/\r\n\r\nhttp://www.cowhorse.com.br/ http://www.lcountrycorrea.com.br http://www.countrystyle.com.br/\r\n\r\nhttp://www.oklahomacountry.com.br/ http://www.betterhorse.com.br/\r\n\r\nhttp://biancardishop.mercadoshops.com.br/ https://www.showhorse.com.br/ http://texas10.com.br\r\n\r\nhttp://americantexas.com.br http://www.correariacampana.com.br https://www.indiretasbrutas.com.br/diversos', 'CriseCia', '', 'publish', 'closed', 'closed', '', 'crisecia', '', '', '2019-05-15 17:18:54', '2019-05-15 20:18:54', '', 0, 'http://localhost/projetos/handgran_portal/?post_type=cliente&#038;p=25', 0, 'cliente', '', 0),
(26, 1, '2019-05-14 17:12:03', '2019-05-14 20:12:03', '', 'CRIS_CIA_LOGO', '', 'inherit', 'open', 'closed', '', 'cris_cia_logo', '', '', '2019-05-14 17:12:03', '2019-05-14 20:12:03', '', 0, 'http://localhost/projetos/handgran_portal/wp-content/uploads/2019/05/CRIS_CIA_LOGO.png', 0, 'attachment', 'image/png', 0),
(34, 1, '2019-05-15 16:51:44', '2019-05-15 19:51:44', '{{unknown}}', '', '', 'publish', 'closed', 'closed', '', '2d32ef55cc3631f6a7ed61850c7d8abf', '', '', '2019-05-15 16:51:44', '2019-05-15 19:51:44', '', 0, 'http://localhost/projetos/handgran_portal/2d32ef55cc3631f6a7ed61850c7d8abf/', 0, 'oembed_cache', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_redirection_404`
--

DROP TABLE IF EXISTS `hp_redirection_404`;
CREATE TABLE IF NOT EXISTS `hp_redirection_404` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `agent` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referrer` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created` (`created`),
  KEY `url` (`url`(191)),
  KEY `referrer` (`referrer`(191)),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_redirection_groups`
--

DROP TABLE IF EXISTS `hp_redirection_groups`;
CREATE TABLE IF NOT EXISTS `hp_redirection_groups` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tracking` int(11) NOT NULL DEFAULT '1',
  `module_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `status` enum('enabled','disabled') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'enabled',
  `position` int(11) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_redirection_groups`
--

INSERT INTO `hp_redirection_groups` (`id`, `name`, `tracking`, `module_id`, `status`, `position`) VALUES
(1, 'Redirecionamentos', 1, 1, 'enabled', 0),
(2, 'Posts modificados', 1, 1, 'enabled', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_redirection_items`
--

DROP TABLE IF EXISTS `hp_redirection_items`;
CREATE TABLE IF NOT EXISTS `hp_redirection_items` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `match_url` varchar(2000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `match_data` text COLLATE utf8mb4_unicode_520_ci,
  `regex` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `position` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `last_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `last_access` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `group_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('enabled','disabled') COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'enabled',
  `action_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `action_code` int(11) UNSIGNED NOT NULL,
  `action_data` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `match_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`),
  KEY `url` (`url`(191)),
  KEY `status` (`status`),
  KEY `regex` (`regex`),
  KEY `group_idpos` (`group_id`,`position`),
  KEY `group` (`group_id`),
  KEY `match_url` (`match_url`(191))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_redirection_logs`
--

DROP TABLE IF EXISTS `hp_redirection_logs`;
CREATE TABLE IF NOT EXISTS `hp_redirection_logs` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `url` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `sent_to` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `agent` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `referrer` mediumtext COLLATE utf8mb4_unicode_520_ci,
  `redirection_id` int(11) UNSIGNED DEFAULT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `module_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created` (`created`),
  KEY `redirection_id` (`redirection_id`),
  KEY `ip` (`ip`),
  KEY `group_id` (`group_id`),
  KEY `module_id` (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_termmeta`
--

DROP TABLE IF EXISTS `hp_termmeta`;
CREATE TABLE IF NOT EXISTS `hp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_terms`
--

DROP TABLE IF EXISTS `hp_terms`;
CREATE TABLE IF NOT EXISTS `hp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_terms`
--

INSERT INTO `hp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'E-commerce', 'e-commerce', 0),
(3, 'Institucional', 'institucional', 0),
(4, 'Marketing Place', 'marketing-place', 0),
(5, 'Material Gráfico', 'material-grafico', 0),
(6, 'Loja Virtual', 'loja-virtual', 0),
(7, 'Marca', 'marca', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_term_relationships`
--

DROP TABLE IF EXISTS `hp_term_relationships`;
CREATE TABLE IF NOT EXISTS `hp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_term_relationships`
--

INSERT INTO `hp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(29, 2, 0),
(29, 6, 0),
(29, 4, 0),
(25, 2, 0),
(25, 3, 0),
(25, 6, 0),
(25, 7, 0),
(25, 4, 0),
(25, 5, 0),
(31, 2, 0),
(31, 3, 0),
(31, 7, 0),
(31, 4, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_term_taxonomy`
--

DROP TABLE IF EXISTS `hp_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `hp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_term_taxonomy`
--

INSERT INTO `hp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'categoriaCliente', '', 0, 3),
(3, 3, 'categoriaCliente', '', 0, 2),
(4, 4, 'categoriaCliente', '', 0, 3),
(5, 5, 'categoriaCliente', '', 0, 1),
(6, 6, 'categoriaCliente', '', 0, 2),
(7, 7, 'categoriaCliente', '', 0, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_usermeta`
--

DROP TABLE IF EXISTS `hp_usermeta`;
CREATE TABLE IF NOT EXISTS `hp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_usermeta`
--

INSERT INTO `hp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'handgran'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'hp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'hp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(26, 1, 'session_tokens', 'a:2:{s:64:\"90c0c304fcad35672a17abc92e8b814c7c602d1f16b4076946e4e6e19129d8f3\";a:4:{s:10:\"expiration\";i:1558532163;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36\";s:5:\"login\";i:1558359363;}s:64:\"5206ab0a48be6b7dfc8d51f11e0cd1fed1e5c49b2b0462c23ac4a4011e4ebfc4\";a:4:{s:10:\"expiration\";i:1558533629;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36\";s:5:\"login\";i:1558360829;}}'),
(17, 1, 'hp_dashboard_quick_press_last_post_id', '28'),
(18, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(19, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(20, 1, 'closedpostboxes_cliente', 'a:2:{i:0;s:23:\"detalhesMetaboxEndereco\";i:1;s:26:\"detalhesMetaboxResponsavel\";}'),
(29, 2, 'nickname', 'contato@handgran.com'),
(30, 2, 'first_name', 'Handgran Usuário'),
(21, 1, 'metaboxhidden_cliente', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(27, 1, 'hp_user-settings', 'libraryContent=browse'),
(28, 1, 'hp_user-settings-time', '1557788936'),
(31, 2, 'last_name', ''),
(32, 2, 'description', ''),
(33, 2, 'rich_editing', 'true'),
(34, 2, 'syntax_highlighting', 'true'),
(35, 2, 'comment_shortcuts', 'false'),
(36, 2, 'admin_color', 'fresh'),
(37, 2, 'use_ssl', '0'),
(38, 2, 'show_admin_bar_front', 'true'),
(39, 2, 'locale', ''),
(40, 2, 'hp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(41, 2, 'hp_user_level', '0'),
(42, 2, 'dismissed_wp_pointers', ''),
(43, 2, 'session_tokens', 'a:3:{s:64:\"d3e52dab4c65ba4b17f5e55d34003ac2904801f60e109158024ccf7a3b422c2f\";a:4:{s:10:\"expiration\";i:1558038419;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36\";s:5:\"login\";i:1557865619;}s:64:\"5682243d3a7539ab5dc3b2c19fdee8efa0eda3e51c758d63b90a53017a7e912c\";a:4:{s:10:\"expiration\";i:1558095895;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36\";s:5:\"login\";i:1557923095;}s:64:\"8ff5ee3715ccd37d281742c0379ae356babde8cc01557155d7bc4ceb030796fe\";a:4:{s:10:\"expiration\";i:1558105879;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36\";s:5:\"login\";i:1557933079;}}'),
(44, 2, 'hp_dashboard_quick_press_last_post_id', '27'),
(45, 2, 'closedpostboxes_dashboard', 'a:2:{i:0;s:18:\"dashboard_activity\";i:1;s:17:\"dashboard_primary\";}'),
(46, 2, 'metaboxhidden_dashboard', 'a:2:{i:0;s:18:\"dashboard_activity\";i:1;s:17:\"dashboard_primary\";}'),
(47, 2, 'hp_user-settings', 'mfold=f'),
(48, 2, 'hp_user-settings-time', '1557923167');

-- --------------------------------------------------------

--
-- Estrutura da tabela `hp_users`
--

DROP TABLE IF EXISTS `hp_users`;
CREATE TABLE IF NOT EXISTS `hp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `hp_users`
--

INSERT INTO `hp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'handgran', '$P$B39TlFFUswoyDei7JFIlxIZfDe0Gqe1', 'handgran', 'hudson@handgran.com', '', '2019-05-08 13:30:53', '', 0, 'handgran'),
(2, 'contato@handgran.com', '$P$B7BNysYiZqVkY0YK6gyyGzlK9rwsRX.', 'contatohandgran-com', 'contato@handgran.com', '', '2019-05-14 20:26:39', '1557865600:$P$BSXiyxaqv332xybYDDliORUk5Di.bW.', 0, 'Handgran Usuário');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
